<?php

namespace App;

use App\Subscriber;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MailsExport implements FromCollection, WithMapping, WithHeadings
{
    public function collection()
    {
        return Subscriber::all();
    }

    public function headings(): array
    {
        return [
            'email',
            'created_at',
        ];
    }

    /**
    * @var Product $subscriber
    */
    public function map($subscriber): array
    {
        return [
            $subscriber->email,
            $subscriber->created_at,
        ];
    }
}
