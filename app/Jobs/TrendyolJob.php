<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
//use Illuminate\Support\Facades\Http;
use App\Brand;
use Illuminate\Support\Str;
use DB;
use App\Upload;
use Illuminate\Http\Request;
use App\Product;
use App\User;
use App\Category;
use App\CategoryTranslation;
use App\Currency;
use App\ProductStock;

class TrendyolJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $url;

    private $cate_array = array();
    

    public function __construct(Request $request)
    {
        $this->url = $request->url;

        
        $women  = array(
            '/elbise-x-c56',
             '/kadin-t-shirt-x-g1-c73',
              '/kadin-gomlek-x-g1-c75',
               '/kadin-jean-x-g1-c120',
                '/kot-ceket-y-s12676',
            '/kadin-pantolon-x-g1-c70',
             '/kadin-mont-x-g1-c118',
              '/kadin-bluz-x-g1-c1019',
               '/kadin-ceket-x-g1-c1030',
                '/etek-x-c69',
                 '/kadin-kazak-x-g1-c1092',
                  '/kadin-tesettur-giyim-x-g1-c81',
                   '/kadin-buyuk-beden-x-g1-c80',
            '/kadin-topuklu-ayakkabi-x-g1-c107',
             '/kadin-sneaker-x-g1-c1172',
              '/kadin-gunluk-ayakkabi-x-g1-c1352',
               '/kadin-babet-x-g1-c113',
                '/kadin-sandalet-x-g1-c111',
                 '/kadin-terlik-x-g1-c110',
            '/kadin-spor-sweatshirt-x-g1-c101456',
             '/kadin-spor-t-shirt-x-g1-c101459',
            '/kadin-spor-tayt-x-g1-c101460',
             '/kadin-esofman-x-g1-c1049',
              '/kadin-spor-ayakkabi-x-g1-c109',
            '/kadin-outdoor-ayakkabi-x-g1-c1128',
             '/kadin-kar-botu-x-g1-c142587',
             '/kadin-pijama-takimi-x-g1-c101496'
        );


        $boy  = array(
            '/erkek-t-shirt-x-g2-c73',
             '/erkek-sort-x-g2-c119',
              '/erkek-gomlek-x-g2-c75',
               '/erkek-esofman-x-g2-c1049',
                '/erkek-pantolon-x-g2-c70',
                 '/erkek-ceket-x-g2-c1030',
            '/erkek-jean-x-g2-c120',
             '/erkek-yelek-x-g2-c1207',
              '/erkek-kazak-x-g2-c1092',
               '/erkek-mont-x-g2-c118',
                '/erkek-takim-elbise-x-g2-c78',
            '/erkek-sweatshirt-x-g2-c1179',
             '/forma-x-c1054',
              '/erkek-spor-ayakkabi-x-g2-c109',
               '/erkek-gunluk-ayakkabi-x-g2-c1352',
                '/erkek-yuruyus-ayakkabisi-x-g2-c101429',
            '/krampon-x-c144727',
             '/erkek-sneaker-x-g2-c1172',
              '/erkek-klasik-ayakkabi-x-g2-c101421',
               '/erkek-kosu-ayakkabisi-x-g2-c101426',
            '/erkek-esofman-x-g2-c1049',
             '/erkek-spor-ayakkabi-x-g2-c109',
              '/erkek-spor-t-shirt-x-g2-c101459',
               '/erkek-spor-sweatshirt-x-g2-c101456',
                '/forma-x-c1054',
            '/erkek-spor-corap-x-g2-c109035',
             '/erkek-spor-giyim-x-g2-c101447',
              '/erkek-outdoor-ayakkabi-x-g2-c1128',
               '/erkek-bot-x-g2-c1025',
                '/sr/erkek-ayakkabi-x-g2-c114?fl=luks'
        );


        $child = array(
            '/bebek-takimlari-x-c103727',
             '/sr/cocuk-ayakkabi-x-g3-c114?gag=2-1,1-1',
              '/hastane-cikisi-x-c104159',
               '/yenidogan-bebek-kiyafetleri-y-s8230',
            '/sr/cocuk-tulum-salopet-x-g3-c104150?gag=2-1,1-1',
             '/bebek-body-zibin-x-c104566',
              '/sr/cocuk-t-shirt-x-g3-c73?gag=2-1,1-1',
               '/sr/cocuk-elbise-x-g3-c56?gag=2-1,1-1',
            '/sr/cocuk-sort-x-g3-c119?gag=2-1,1-1',
             '/bebek-patigi-y-s7272',
            '/sr/cocuk-elbise-x-g3-c56?gag=1-2',
             '/sr/cocuk-sweatshirt-x-g3-c1179?gag=1-2',
              '/sr/cocuk-spor-ayakkabi-x-g3-c109?gag=1-2',
               '/sr/cocuk-esofman-x-g3-c1049?gag=1-2',
            '/sr/cocuk-ic-giyim-x-g3-c64?gag=1-2',
             '/sr/cocuk-t-shirt-x-g3-c73?gag=1-2',
              '/sr/cocuk-tayt-x-g3-c121?gag=1-2',
               '/sr/cocuk-gunluk-ayakkabi-x-g3-c1352?gag=1-2',
            '/sr/cocuk-sort-x-g3-c119?gag=1-2',
             '/sr/cocuk-mont-x-g3-c118?gag=1-2',
              '/sr/cocuk-sweatshirt-x-g3-c1179?gag=2-2',
            '/sr/cocuk-spor-ayakkabi-x-g3-c109?gag=2-2',
             '/sr/cocuk-esofman-x-g3-c1049?gag=2-2',
              '/sr/cocuk-ic-giyim-x-g3-c64?gag=2-2',
               '/sr/cocuk-t-shirt-x-g3-c73?gag=2-2',
            '/sr/cocuk-gunluk-ayakkabi-x-g3-c1352?gag=2-2',
             '/sr/cocuk-sort-x-g3-c119?gag=2-2',
              '/sr/cocuk-gomlek-x-g3-c75?gag=2-2',
               '/sr/cocuk-mont-x-g3-c118?gag=2-2'
        );

        $accessory = array(
            '/kadin-aksesuar-x-g1-c27',
            '/erkek-aksesuar-x-g2-c27'
        );


        if($request->url == 'women')
        {
            $this->cate_array = $women;
        }
        elseif($request->url == 'boy')
        {
            $this->cate_array = $boy;
        }
        elseif($request->url == 'child')
        {
            $this->cate_array = $child;
        }
        elseif($request->url == 'accessory')
        {
            $this->cate_array = $accessory;
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        foreach ($this->cate_array as $key => $cate_url) {
            $first = 'https://public.trendyol.com/discovery-web-searchgw-service/v2/api/infinite-scroll'.$cate_url.'?pi=';
            $second = '&storefrontId=1&culture=tr-TR&userGenderId=1&pId=0&scoringAlgorithmId=2&categoryRelevancyEnabled=false&isLegalRequirementConfirmed=true&searchStrategyType=DEFAULT&productStampType=TypeA&searchTestTypeAbValue=A';
            $i = 1;
            while($i <= 1){
                $url_pg = $first . $i . $second;
                $client = new \GuzzleHttp\Client();
                $request = $client->get($url_pg);
                $result = json_decode($request->getBody());
                if($result->error){
                    breake;
                }
                foreach($result->result->products as $product){
                    DB::transaction(function () use($product){
                        $pro = Product::where('name',$product->name)->where('unit_price','>',0)->first();
                        if(!$pro){
                            $pro = new Product;
                            $pro->name = $product->name;
                            if($this->url != 'accessory'){
                                $pro->attributes = '["1"]';
                                $pro->choice_options = '[{"attribute_id":"1","values":["s","m","l","xl","xxl","30","32","34","36","38","40","42","44"]}]';
                                $pro->colors = '["#FF0000","#FFFF00","#00FF00","#00FFFF","#FFFFFF","#000000","#0000FF"]';
                                $array_option = array("s","m","l","xl","xxl","30","32","34","36","38","40","42","44");
                                $array_color = array("#FF0000","#FFFF00","#00FF00","#00FFFF","#FFFFFF","#000000","#0000FF");
                            }
                            else{
                                $pro->attributes = '[]';
                                $pro->choice_options = '[]';
                                $pro->colors = '[]';
    
                            }

                            $pro->meta_title = $product->name;
                            $pro->published = 0;
                            $pro->cash_on_delivery = 0;
                            $pro->slug = $product->imageAlt;
                            $pro->photos = ""; 
                            $pro->thumbnail_img = ""; 
                            foreach ($product->images as $key => $value) {
                                if($key == 4)
                                {
                                    break;
                                }
                                try{
                                    $extension = pathinfo("https://cdn.dsmcdn.com/mnresize/250/500" . $value, PATHINFO_EXTENSION);
                                    $filename = 'uploads/all/'.Str::random(5).'.'.$extension;
                                    $fullpath = 'public/'.$filename;
                                    $file = file_get_contents("https://cdn.dsmcdn.com/mnresize/250/500" . $value);
                                    file_put_contents($fullpath, $file);

                                    $upload = new Upload;
                                    $upload->extension = strtolower($extension);

                                    $upload->file_original_name = $filename;
                                    $upload->file_name = $filename;
                                    $upload->user_id = 9;
                                    $upload->type = "image";
                                    $upload->file_size = filesize(base_path($fullpath));
                                    $upload->save();
                                    if(!$pro->thumbnail_img){
                                        $pro->photos = $upload->id;
                                        $pro->thumbnail_img  = $upload->id;
                                    }
                                    else{
                                        $pro->photos = $pro->photos .",". $upload->id;
                                    }
                                }
                                catch(\Exception $e){

                                }

                            }
                                /*$image = file_get_contents("https://cdn.dsmcdn.com/mnresize/250/500 " . $value);
                                $upload = new Upload;
                                $upload->user_id = 9;
                                $upload->type = "image";
                                $upload->extension = "jpg";
                                $upload->file_original_name = "trendyol-"+substr($value, strrpos($vlaue, '/') + 1);
                                $upload->file_name = "trendyol-"+substr($value, strrpos($vlaue, '/') + 1);
                                $file->move('uploads/all/', $upload->file_original_name);
                                $upload->save();
                                $pro->photos += $upload->id;
                                if($pro->thumbnail_img == ""){
                                    $pro->thumbnail_img += $upload->id;
                                }
                            }*/
                            //pricing 

                            $pro->shipping_type = 'product_wise';
                            $currency = Currency::where('symbol','Lt')->first();
                            $pro->unit_price = $product->price->originalPrice * $currency->exchange_rate;
                            $pro->purchase_price = $product->price->originalPrice * $currency->exchange_rate;
                            if($pro->unit_price < 9){
                                $pro->unit_price *= 2;
                                $pro->purchase_price *= 2;
                            }
                            elseif($pro->unit_price >= 9 && $pro->unit_price < 16){
                                $pro->unit_price += $pro->unit_price * 0.9;
                                $pro->purchase_price += $pro->purchase_price * 0.9;
                            }
                            elseif($pro->unit_price >= 16 && $pro->unit_price < 23){
                                $pro->unit_price += $pro->unit_price * 0.8;
                                $pro->purchase_price += $pro->purchase_price * 0.8;
                            }
                            elseif($pro->unit_price >= 23 && $pro->unit_price < 30){
                                $pro->unit_price += $pro->unit_price * 0.75;
                                $pro->purchase_price += $pro->purchase_price * 0.75;
                            }
                            else{
                                $pro->unit_price += $pro->unit_price * 0.7;
                                $pro->purchase_price += $pro->purchase_price * 0.7;
                            }

                            //shipping cost

                            if($pro->unit_price < 20){
                                $pro->shipping_cost = 15;    
                            }
                            elseif($pro->unit_price < 40){
                                $pro->shipping_cost = 18;    
                            }
                            elseif($pro->unit_price < 75){
                                $pro->shipping_cost = 22;    
                            }
                            else{
                                $pro->shipping_cost = 27;    
                            }
                            


                            $pro->added_by = "admin";
                            $pro->trendyol_url = 'https://www.trendyol.com'.$product->url;
                            $pro->user_id = 9;
                            $brand = Brand::where('name',$product->brand->name)->first();
                            if(!$brand){
                                $brand = new Brand;
                                $brand->name = $product->brand->name;
                                $brand->slug = $product->brand->name;
                                $brand->meta_title = $product->brand->name;
                                $brand->logo = "uploads/brands/brand.jpg";
                                $brand->top = 1;
                                $brand->save();
                            }
                            $cate_var = explode("/", $product->categoryHierarchy);
                            $father = Category::where('name',$cate_var[0])->first();
                            if(!$father){
                                $father = new Category;
                                $father->name = $cate_var[0];
                                $father->meta_title = $cate_var[0];
                                $father->slug = $cate_var[0];
                                $father->parent_id = 0;
                                $father->level = 0;
                                $father->commision_rate = 0;
                                $father->featured = 0;
                                $father->top = 1;
                                $father->digital = 0;
                                $father->banner = 27;
                                $father->icon = 25;
                                $father->commision_rate = 0;
                                $father->save();
                                $cate_en = new CategoryTranslation;
                                $cate_en->name = $cate_var[0];
                                $cate_en->lang = 'en';
                                $cate_en->name = $father->id;
                            }
                            $perant = Category::where('name',$cate_var[1])->first();
                            if(!$perant){
                                $perant = new Category;
                                $perant->name = $cate_var[1];
                                $perant->meta_title = $cate_var[1];
                                $perant->slug = $cate_var[1];
                                $perant->parent_id = $father->id;
                                $perant->level = 1;
                                $perant->commision_rate = 0;
                                $perant->featured = 0;
                                $perant->top = 1;
                                $perant->digital = 0;
                                $perant->banner = 27;
                                $perant->icon = 25;
                                $perant->commision_rate = 0;
                                $perant->save();

                                $cate_en = new CategoryTranslation;
                                $cate_en->name = $cate_var[1];
                                $cate_en->lang = 'en';
                                $cate_en->name = $perant->id;
                            }
                            if(count($cate_var) > 2){
                            $section = Category::where('name',$cate_var[2])->first();
                            if(!$section){
                                $section = new Category;
                                $section->name = $cate_var[2];
                                $section->meta_title = $cate_var[2];
                                $section->slug = $cate_var[2];
                                $section->parent_id = $perant->id;
                                $section->level = 2;
                                $section->commision_rate = 0;
                                $section->featured = 0;
                                $section->top = 1;
                                $section->digital = 0;
                                $section->banner = 27;
                                $section->icon = 25;
                                $section->commision_rate = 0;
                                $section->save();
                                $cate_en = new CategoryTranslation;
                                $cate_en->name = $cate_var[2];
                                $cate_en->lang = 'en';
                                $cate_en->name = $section->id;
                            }
                            $pro->category_id = $section->id;
                            }
                            else{
                                $pro->category_id = $perant->id;
                            }
                            $pro->brand_id = $brand->id;
                            $pro->current_stock = 1000;
                            $pro->min_qty = 1;
                            $pro->save();
                            if($this->url != 'accessory'){
                                foreach ($array_option as $key => $value) {
                                    $product_stock = new ProductStock;
                                    $product_stock->product_id = $pro->id;  
                                    $product_stock->variant = $value;
                                    $product_stock->price = $pro->unit_price;
                                    $product_stock->sku = str_replace(' ', '_', $pro->slug). "value";
                                    $product_stock->qty = 1000;
                                    // $product_stock->image = $pro->thumbnail_img;
                                    $product_stock->save();    
                                }
                                foreach ($array_color as $key => $value) {
                                    $product_stock = new ProductStock;
                                    $product_stock->product_id = $pro->id;  
                                    $product_stock->variant = $value;
                                    $product_stock->price = $pro->unit_price;
                                    $product_stock->sku = str_replace(' ', '_', $pro->slug). "value";
                                    $product_stock->qty = 1000;
                                    // $product_stock->image = $pro->thumbnail_img;
                                    $product_stock->save();    
                                }
                            }
                            else{
                                $product_stock = new ProductStock;
                                $product_stock->product_id = $pro->id;  
                                // $product_stock->variant = $value;
                                $product_stock->price = $pro->unit_price;
                                $product_stock->sku = str_replace(' ', '_', $pro->slug). "value";
                                $product_stock->qty = 1000;
                                // $product_stock->image = $pro->thumbnail_img;
                                $product_stock->save();
                            }
                        }

                    });
                }
                $i++;
            }
        }
    }
}
