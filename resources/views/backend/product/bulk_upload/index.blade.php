@extends('backend.layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6">{{translate('Product Bulk Upload')}}</h5>
        </div>
        <div class="card-body">
            <div class="alert" style="color: #004085;background-color: #cce5ff;border-color: #b8daff;margin-bottom:0;margin-top:10px;">
                <strong>{{ translate('Step 1')}}:</strong>
                <p>1. {{translate('Download the skeleton file and fill it with proper data')}}.</p>
                <p>2. {{translate('You can download the example file to understand how the data must be filled')}}.</p>
                <p>3. {{translate('Once you have downloaded and filled the skeleton file, upload it in the form below and submit')}}.</p>
                <p>4. {{translate('After uploading products you need to edit them and set product\'s images and choices')}}.</p>
            </div>
            <br>
            <div class="">
                <a href="{{ static_asset('download/product_bulk_demo.xlsx') }}" download><button class="btn btn-info">{{ translate('Download CSV')}}</button></a>
            </div>
            <div class="alert" style="color: #004085;background-color: #cce5ff;border-color: #b8daff;margin-bottom:0;margin-top:10px;">
                <strong>{{translate('Step 2')}}:</strong>
                <p>1. {{translate('Category and Brand should be in numerical id')}}.</p>
                <p>2. {{translate('You can download the pdf to get Category and Brand id')}}.</p>
            </div>
            <br>
            <div class="">
                <a href="{{ route('pdf.download_category') }}"><button class="btn btn-info">{{translate('Download Category')}}</button></a>
                <a href="{{ route('pdf.download_brand') }}"><button class="btn btn-info">{{translate('Download Brand')}}</button></a>
                @if(\Queue::size()==0)
                    <a href="{{ route('trendyol.woman') }}"><button class="btn btn-success">{{translate('Trendyol Woman')}}</button></a>
                    <a href="{{ route('trendyol.man') }}"><button class="btn btn-success">{{translate('Trendyol Man')}}</button></a>
                    <a href="{{ route('trendyol.child') }}"><button class="btn btn-success">{{translate('Trendyol Child')}}</button></a>
                @else
                    <a href="{{ route('trendyol.woman') }}"><button class="btn btn-danger">{{translate('Trendyol Woman')}}</button></a>
                    <a href="{{ route('trendyol.man') }}"><button class="btn btn-danger">{{translate('Trendyol Man')}}</button></a>
                    <a href="{{ route('trendyol.child') }}"><button class="btn btn-danger">{{translate('Trendyol Child')}}</button></a>
                @endif
            </div>
            <br>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 h6"><strong>{{translate('Upload Product File')}}</strong></h5>
        </div>
        @if (!isset($rows))
        <div class="card-body">
            <form class="form-horizontal" action="{{ route('bulk_product_upload1') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div class="custom-file">
    						<label class="custom-file-label">
    							<input type="file" name="bulk_file" class="custom-file-input" required>
    							<span class="custom-file-name">{{ translate('Choose File')}}</span>
    						</label>
    					</div>
                    </div>
                </div>
                <div class="form-group mb-0">
                    <button type="submit" class="btn btn-info">{{translate('Upload CSV')}}</button>
                </div>
            </form>
        </div>
        @endif
        @if (isset($rows))
    <form class="form-horizontal" action="{{ route('bulk_product_upload') }}" method="POST" enctype="multipart/form-data">
        <div class="container">
            @csrf
            <label for="name">Name:</label>
            <select class="form-control"  name="name" id="name">
                @foreach ($rows as $item)
                <option value="{{$item}}">{{$item}}</option>
                @endforeach
            </select>
            <br>

            <label >Category ID:</label>
            <input type="number" class="form-control"  style="width:35%;display:inline" name="cat_id" id="cat_id">
            <label >Brand ID:</label>
            <input type="number"  class="form-control" style="width:35%;display:inline" name="brand" id="brand">

            <br>


            <label for="name">Unit Price:</label>
            <select class="form-control"  name="unit_price" id="unit_price">
                 @foreach ($rows as $item)
                <option value="{{$item}}">{{$item}}</option>
                @endforeach
            </select>

            <label for="name">Purchase Price:</label>
            <select class="form-control"  name="purchase_price" id="purchase_price">
                 @foreach ($rows as $item)
                <option value="{{$item}}">{{$item}}</option>
                @endforeach
            </select>

            <label for="name">Discount Type:</label>
            <select class="form-control"  name="discount_type" id="discount_type">
                 @foreach ($rows as $item)
                <option value="{{$item}}">{{$item}}</option>
                @endforeach
            </select>

            <label for="name">Discount</label>
            <select class="form-control"  name="discount" id="discount">
                 @foreach ($rows as $item)
                <option value="{{$item}}">{{$item}}</option>
                @endforeach
            </select>


            <label for="name">Unit:</label>
            <select class="form-control"  name="unit" id="unit">
                 @foreach ($rows as $item)
                <option value="{{$item}}">{{$item}}</option>
                @endforeach
            </select>


            <br>


            <label for="name">current_stock:</label>
            <select class="form-control"  name="current_stock" id="current_stock">
                 @foreach ($rows as $item)
                <option value="{{$item}}">{{$item}}</option>
                @endforeach
            </select>

            <label for="name">meta_title:</label>
            <select class="form-control"  name="meta_title" id="meta_title">
                 @foreach ($rows as $item)
                <option value="{{$item}}">{{$item}}</option>
                @endforeach
            </select>

            <label for="name">meta_description:</label>
            <select class="form-control"  name="meta_description" id="meta_description">
                 @foreach ($rows as $item)
                <option value="{{$item}}">{{$item}}</option>
                @endforeach
            </select>


            <br>




            <label for="name">thumbnail_img:</label>
            <select class="form-control"  name="thumbnail_img" id="thumbnail_img">
                 @foreach ($rows as $item)
                <option value="{{$item}}">{{$item}}</option>
                @endforeach
            </select>

            <label for="name">image:</label>
            <select class="form-control"  name="image" id="image">
                 @foreach ($rows as $item)
                <option value="{{$item}}">{{$item}}</option>
                @endforeach
            </select>
            <br>
            <div class="form-group row">
                <div class="col-sm-9">
                    <div class="custom-file">
                        <label class="custom-file-label">
                            <input type="file" name="bulk_file" class="custom-file-input" required>
                            <span class="custom-file-name">{{ translate('Choose File')}}</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group mb-0">
                <button type="submit" class="btn btn-info">{{translate('Upload CSV')}}</button>
            </div>
            <br><br>
        </div>


        </form>
        @endif










    </div>

@endsection
